# Personal project

This project is created by Soheil Movahedan, feel free to leave
a comment or even better! A Pull request and make it better.

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

### Todo
	- Deploy on vercel
	-	Add storybook
	-	Add headless-ui
	-	Jest mocking service
	- Develop server-side, Structure for features/models/queries
	- Develop react-query
	-	Complete Authentication
	-	Blog
	- Introduce to machine learning and create an example

### Done
	-	Connect with mongoose
	-	Error Handling and Sentry!
	-	MediaQuery, Agent, tailwindConfig

# Features

	- Authentication
	- Notification
	- Blog (with comments)
	- Show this codebase
	- 
	