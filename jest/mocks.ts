// example mock data that will replace API data
export const mockedData = [
	{
		id: 1,
		name: 'one',
		description: 'one',
	},
	{
		id: 2,
		name: 'two',
		description: 'two',
	},
];
