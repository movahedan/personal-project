import { create } from '@storybook/theming';

export default create({
  base: 'light',

  colorSecondary: '#e01a22',

  brandTitle: 'personal-project storybook',
  brandUrl: '',
  brandImage: '',
});
